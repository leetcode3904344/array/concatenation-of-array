//
// Created by Vo Dinh Nguyen  on 03/11/2023.
//

#include "stdio.h"
#include "mm_malloc.h"

int* getConcatenation(int* nums, int numsSize, int* returnSize){
    int *newArr = (int*) malloc(numsSize*sizeof(newArr));
    *returnSize = 2*numsSize;
    for(int i = 0; i < numsSize; i++){
        newArr[i] = nums[i];
        newArr[i+numsSize] = nums[i];
    }
    return newArr;
}

int main(){
    int* nums;
    int numsSize;
    int returnSize;
    nums = (int*) malloc(numsSize*sizeof(nums));
    printf("\nEnter the element in array: ");
    scanf("%d", &numsSize);

    printf("\nEnter the nums in array: ");
    for(int i = 0; i < numsSize; i++){
        scanf("%d", &nums[i]);
    }

    int *ans = getConcatenation(nums, numsSize, &returnSize);

    for(int i = 0; i < returnSize; i++) {
        printf("%d ", ans[i]);
    }

    free(nums);
    free(ans);
    return 0;
}
